  -v  : print version number and exit
  -exit : exit immediatly
  -format  <filename> : check file format
  -test_ip <ip> : undocumented
  -check_impl  : display information on the implementations
  -stdout : keep output to stdout after startup
  -stderr : keep output to stderr after startup
  -daemon  : this argument was removed, core will exit
  -find_port  : find another port when one is already used
  -pid : directory for pid file
  -useradd "<user> <pass>" : create user/change password
  -client_name <string> : 	small name of client (current: fsytyh)
  -allowed_ips <string> : 	list of IP address allowed to connect to the core via telnet/GUI/WEB
  for internal command set: list separated by spaces
  example for internal command: set allowed_ips "127.0.0.0/8 192.168.1.2"
  or for editing the ini-file: list separated by semi-colon
  example for ini-file: allowed_ips = [ "127.0.0.0/8"; "192.168.1.2";]
  CIDR and range notations are supported: ie use 192.168.0.0/24
  or 192.168.0.0-192.168.0.255 for 192.168.0.* (current: 127.0.0.1)
  -gui_port <string> : 	port for Graphical Interfaces, 0 to deactivate GUI interface (current: 4001)
  -gift_port <string> : 	port for GiFT Graphical Interfaces interaction. It was 1213, but the default is
  now 0 for disabled, because it does not check for a password. (current: 0)
  -http_port <string> : 	The port used to connect to your client with a web browser, 0 to deactivate web interface (current: 4080)
  -telnet_port <string> : 	port for user interaction, 0 to deactivate telnet interface (current: 4000)
  -http_bind_addr <string> : 	The IP address used to bind the http server (current: 0.0.0.0)
  -gui_bind_addr <string> : 	The IP address used to bind the gui server (current: 0.0.0.0)
  -telnet_bind_addr <string> : 	The IP address used to bind the telnet server (current: 0.0.0.0)
  -print_all_sources <string> : 	Should *all* sources for a file be shown on HTML/telnet vd <num> (current: true)
  -improved_telnet <string> : 	Improved telnet interface (current: true)
  -verbosity <string> : 	A space-separated list of keywords. Each keyword triggers
  printing information on the corresponding messages:
  verb : verbose mode (interesting not only for coders)
  mc : debug client messages
  mr|raw : debug raw messages
  mct : debug emule clients tags
  ms : debug server messages
  sm : debug source management
  net : debug net
  gui : debug gui
  no-login : disable login messages
  file : debug file handling
  do : some download warnings
  up : some upload warnings
  unk : unknown messages
  ov : overnet
  loc : debug source research/master servers
  share: debug sharing
  md4 : md4 computation
  connect : debug connections
  udp : udp messages
  ultra|super : debug supernode
  swarming : debug swarming
  hc : http_client messages
  hs : http_server messages
  com : commands by non-admin users
  act : debug activity
  bw : debug bandwidth
  geo : debug GeoIP
  unexp : debug unexpected messages (current: )
  -max_hard_upload_rate <string> : 	The maximal upload rate you can tolerate on your link in kBytes/s (0 = no limit)
  The limit will apply on all your connections (clients and servers) and both
  control and data messages. (current: 10)
  -max_hard_download_rate <string> : 	The maximal download rate you can tolerate on your link in kBytes/s (0 = no limit)
  The limit will apply on all your connections (clients and servers) and both
  control and data messages. Maximum value depends on max_hard_upload_rate:
  >= 10    -> unlimited download
  < 10 > 3 -> download limited to upload * 4
  < 4      -> download limited to upload * 3 (current: 50)
  -max_hard_upload_rate_2 <string> : 	Second maximal upload rate for easy toggling (use bw_toggle) (current: 5)
  -max_hard_download_rate_2 <string> : 	Second maximal download rate for easy toggling (use bw_toggle) (current: 20)
  -max_opened_connections <string> : 	Maximal number of opened connections (current: 200)
  -max_opened_connections_2 <string> : 	Second maximal number of opened connections for easy toggling (use bw_toggle) (current: 100)
  -max_indirect_connections <string> : 	Amount of indirect connections in percent (min 30, max 70) of max_opened_connections (current: 30)
  -max_upload_slots <string> : 	How many slots can be used for upload, minimum 3 (current: 5)
  -max_release_slots <string> : 	How many percent of upload slots can be used for downloading files
  tagged as release, maximum 75% (current: 20)
  -friends_upload_slot <string> : 	Set aside a single reserved slot to upload to friends (current: true)
  -small_files_slot_limit <string> : 	Maximum file size to benefit from the reserved slot for small files (0 to disable) (current: 10240)
  -dynamic_slots <string> : 	Set this to true if you want to have dynamic upload slot allocation (experimental) (current: false)
  -max_connections_per_second <string> : 	Maximal number of connections that can be opened per second (current: 5)
  -loop_delay <string> : 	The delay in milliseconds to wait in the event loop. Can be decreased to
  increase the bandwidth usage, or increased to lower the CPU usage. (current: 5)
  -nolimit_ips <string> : 	list of IP addresses allowed to connect to the core with no limit on
  upload/download and upload slots.  List separated by spaces, wildcard=255
  ie: use 192.168.0.255 for 192.168.0.*  (current: 127.0.0.1 )
  -copy_read_buffer <string> : 	This option enables MLdonkey to always read as much data as possible
  from a channel, but use more CPU as it must then copy the data in the
  channel buffer. (current: true)
  -enable_overnet <string> : 	Set to true if you also want mldonkey to run as an overnet client
  (enable_donkey must be true) (current: true)
  -enable_kademlia <string> : 	Set to true if you also want mldonkey to run as an kademlia client
  (enable_donkey must be true, and only experimental) (current: false)
  -enable_servers <string> : 	Set to true if you want mldonkey to connect to edonkey servers
  (enable_donkey must be true, and only experimental) (current: true)
  -enable_bittorrent <string> : 	Set to true if you also want mldonkey to run as an Bittorrent client (current: true)
  -enable_donkey <string> : 	Set to true if you also want mldonkey to run as a donkey client (current: true)
  -enable_opennap <string> : 	Set to true if you also want mldonkey to run as a napster client (experimental) (current: false)
  -enable_soulseek <string> : 	Set to true if you also want mldonkey to run as a soulseek client (experimental) (current: false)
  -enable_gnutella <string> : 	Set to true if you also want mldonkey to run as a gnutella1 sub node (experimental) (current: false)
  -enable_gnutella2 <string> : 	Set to true if you also want mldonkey to run as a gnutella2 sub node (experimental) (current: false)
  -enable_fasttrack <string> : 	Set to true if you also want mldonkey to run as a Fasttrack sub node (experimental) (current: false)
  -enable_directconnect <string> : 	Set to true if you also want mldonkey to run as a direct-connect node (experimental) (current: false)
  -enable_openft <string> : 	Set to true if you also want mldonkey to run as a OpenFT sub node (experimental) (current: false)
  -enable_fileTP <string> : 	Set to true if you also want mldonkey to download HTTP files (experimental) (current: true)
  -client_ip <string> : 	The last IP address used for this client (current: 134.157.168.19)
  -force_client_ip <string> : 	Use the IP specified by 'client_ip' instead of trying to determine it
  ourself. Don't set this option to true if you have dynamic IP. (current: false)
  -discover_ip <string> : 	Use http://ip.discoveryvip.com/ip.asp to obtain WAN IP (current: true)
  -user_agent <string> : 	User agent string (default = "default") (current: default)
  -rss_preprocessor <string> : 	If MLDonkey can not read broken RSS feeds, use this program to preprocess them (current: xmllint)
  -ip_blocking_descriptions <string> : 	Keep IP blocking ranges descriptions in memory (current: false)
  -ip_blocking <string> : 	IP blocking list filename (peerguardian format), can also be in gz/bz2/zip format
  Zip files must contain either a file named guarding.p2p or guarding_full.p2p. (current: )
  -ip_blocking_countries <string> : 	List of countries to block connections from/to (requires Geoip).
  Names are in ISO 3166 format, see http://www.maxmind.com/app/iso3166
  You can also at your own risk use "Unknown" for IPs Geoip won't recognize. (current: )
  -ip_blocking_countries_block <string> : 	false: use ip_blocking_countries as block list, all other countries are allowed
  true: use ip_blocking_countries as allow list, all other countries are blocked (current: false)
  -geoip_dat <string> : 	Location of GeoIP.dat (Get one from http://www.maxmind.com/download/geoip/database/) (current: )
  -tcpip_packet_size <string> : 	The size of the header of a TCP/IP packet on your connection (ppp adds
  14 bytes sometimes, so modify to take that into account) (current: 40)
  -mtu_packet_size <string> : 	The size of the MTU of a TCP/IP packet on your connection (current: 1500)
  -minimal_packet_size <string> : 	The size of the minimal packet you want mldonkey to send when data is
  available on the connection (current: 600)
  -socket_keepalive <string> : 	Should a connection check if the peer we are connected to is still alive?
  This implies some bandwidth-cost (with 200 connections ~10-20%) (current: false)
  -http_proxy_server <string> : 	Direct HTTP queries to HTTP proxy (current: )
  -http_proxy_port <string> : 	Port of HTTP proxy (current: 8080)
  -http_proxy_tcp <string> : 	Direct TCP connections to HTTP proxy (the proxy should support CONNECT) (current: false)
  -html_mods <string> : 	Whether to use the modified WEB interface (current: true)
  -html_mods_style <string> : 	Which html_mods style to use (set with html_mods_style command) (current: 0)
  -html_mods_human_readable <string> : 	Whether to use human readable GMk number format (current: true)
  -html_mods_use_relative_availability <string> : 	Whether to use relative availability in the WEB interface (current: true)
  -html_mods_vd_network <string> : 	Whether to display the Net column in vd output (current: true)
  -html_mods_vd_comments <string> : 	Whether to display the Comments column in vd output (current: true)
  -html_mods_vd_user <string> : 	Whether to display the User column in vd output (current: false)
  -html_mods_vd_group <string> : 	Whether to display the Group column in vd output (current: false)
  -html_mods_vd_active_sources <string> : 	Whether to display the Active Sources column in vd output (current: true)
  -html_mods_vd_age <string> : 	Whether to display the Age column in vd output (current: true)
  -html_flags <string> : 	Whether to display flags instead of country codes (current: true)
  -html_mods_vd_gfx <string> : 	Show graph in vd output (current: true)
  -html_mods_vd_gfx_remove <string> : 	Remove graph files on core shutdown (current: false)
  -html_mods_vd_gfx_fill <string> : 	Fill graph in vd output (current: true)
  -html_mods_vd_gfx_split <string> : 	Split download and upload graph in vd output (current: false)
  -html_mods_vd_gfx_stack <string> : 	Stacked download and upload graph (current: true)
  -html_mods_vd_gfx_flip <string> : 	Flip up/side graph position in vd output (current: true)
  -html_mods_vd_gfx_mean <string> : 	Show mean line on graph in vd output (current: true)
  -html_mods_vd_gfx_transparent <string> : 	Show transparent graph in vd output (only for png) (current: true)
  -html_mods_vd_gfx_png <string> : 	Draw graph as png if true, else draw as jpg in vd output (current: true)
  -html_mods_vd_gfx_h <string> : 	Show hourly graph in vd output (current: true)
  -html_mods_vd_gfx_x_size <string> : 	Graph x size in vd output ( 365 < x < 3665 ) (current: 795)
  -html_mods_vd_gfx_y_size <string> : 	Graph y size in vd output ( 200 < y < 1200 ) (current: 200)
  -html_mods_vd_gfx_h_intervall <string> : 	compute values for hourly graph every 1,2,3,4,5,10,15,20,30,60 min
	Changes to this option require a core restart. (current: 60)
  -html_mods_vd_gfx_h_dymamic <string> : 	Dynamic grid width, start with 1 h/grid, maximum html_mods_vd_gfx_h_grid_time h/grid (current: true)
  -html_mods_vd_gfx_h_grid_time <string> : 	Max hours on time scale per grid (0 = no limit) (current: 0)
  -html_mods_vd_gfx_subgrid <string> : 	Number of shown subgrids on graph (0 = no subgrids) (current: 0)
  -html_mods_vd_gfx_tag <string> : 	Draw tag graph (current: false)
  -html_mods_vd_gfx_tag_use_source <string> : 	Use tag source image  (current: false)
  -html_mods_vd_gfx_tag_source <string> : 	Tag source image name (current: image)
  -html_mods_vd_gfx_tag_png <string> : 	Draw tag as png if true, else draw as jpg in vd output (current: true)
  -html_mods_vd_gfx_tag_enable_title <string> : 	Enable tag graph title (current: true)
  -html_mods_vd_gfx_tag_title <string> : 	Tag graph title (current: MLNet traffic)
  -html_mods_vd_gfx_tag_title_x_pos <string> : 	Tag graph title x pos in vd output (current: 4)
  -html_mods_vd_gfx_tag_title_y_pos <string> : 	Tag graph title y pos in vd output (current: 1)
  -html_mods_vd_gfx_tag_dl_x_pos <string> : 	Tag graph download x pos in vd output (current: 4)
  -html_mods_vd_gfx_tag_dl_y_pos <string> : 	Tag graph download y pos in vd output (current: 17)
  -html_mods_vd_gfx_tag_ul_x_pos <string> : 	Tag graph upload x pos in vd output (current: 4)
  -html_mods_vd_gfx_tag_ul_y_pos <string> : 	Tag graph upload y pos in vd output (current: 33)
  -html_mods_vd_gfx_tag_x_size <string> : 	Tag graph x size in vd output ( 130 < x < 3600 ) (current: 80)
  -html_mods_vd_gfx_tag_y_size <string> : 	Tag graph y size in vd output ( 50 < x < 1200 ) (current: 50)
  -html_mods_vd_last <string> : 	Whether to display the Last column in vd output (current: true)
  -html_mods_vd_prio <string> : 	Whether to display the Priority column in vd output (current: true)
  -html_vd_barheight <string> : 	Change height of download indicator bar in vd output (current: 2)
  -html_vd_chunk_graph <string> : 	Whether to display chunks list as graph or text in vd output (current: true)
  -html_vd_chunk_graph_style <string> : 	Change style of chunk graph (current: 0)
  -html_vd_chunk_graph_max_width <string> : 	Change max width of chunk graph (current: 200)
  -html_mods_show_pending <string> : 	Whether to display the pending slots in uploaders command (current: true)
  -html_mods_load_message_file <string> : 	Whether to load the mldonkey_messages.ini file (false=use internal settings) (current: false)
  -html_mods_max_messages <string> : 	Maximum chat messages to log in memory (current: 50)
  -html_mods_bw_refresh_delay <string> : 	bw_stats refresh delay (seconds) (current: 11)
  -html_mods_theme <string> : 	html_mods_theme to use (located in relative html_themes/<theme_name> directory
  leave blank to use internal theme (current: )
  -html_checkbox_vd_file_list <string> : 	Whether to use checkboxes in the WEB interface for download list (current: true)
  -html_checkbox_search_file_list <string> : 	Whether to use checkboxes in the WEB interface for search result list (current: false)
  -html_use_gzip <string> : 	Use gzip compression on web pages (current: false)
  -html_mods_use_js_tooltips <string> : 	Whether to use the fancy javascript tooltips or plain html-title (current: true)
  -html_mods_js_tooltips_wait <string> : 	How long to wait before displaying the tooltips (current: 0)
  -html_mods_js_tooltips_timeout <string> : 	How long to display the tooltips (current: 100000)
  -html_mods_use_js_helptext <string> : 	Use javascript to display option help text as js popup (true=use js, false=use html tables) (current: true)
  -allow_local_network <string> : 	If this option is set, IP addresses on the local network are allowed
  (only for debugging) (current: false)
  -log_size <string> : 	size of log in number of records (current: 300)
  -log_file_size <string> : 	Maximum size of log_file in MB, this value is only checked on startup,
   log_file will be deleted if its bigger than log_file_size. (current: 2)
  -log_file <string> : 	The file in which you want mldonkey to log its debug messages. If you
  set this option, mldonkey will log this info in the file until you use the
  'close_log' command. The log file may become very large. You can
  also enable logging in a file after startup using the 'log_file' command. (current: mlnet.log)
  -log_to_syslog <string> : 	Post log messages to syslog. This setting is independent of log_file
  and its associated commands, therefore close_log does not stop log to syslog.
  Its therefore possible to log to syslog and log_file at the same time. (current: false)
  -gui_log_size <string> : 	number of lines for GUI console messages (current: 30)
  -auto_commit <string> : 	Set to false if you don't want mldonkey to automatically put completed files
  in incoming directory (current: true)
  -pause_new_downloads <string> : 	Set to true if you want all new downloads be paused immediatly
  will be set to false on core start. (current: false)
  -release_new_downloads <string> : 	Set to true if you want to activate the release slot feature for all new downloads. (current: false)
  -max_concurrent_downloads <string> : 	The maximal number of files in Downloading state (other ones are Queued) (current: 50)
  -sources_per_chunk <string> : 	How many sources to use to download each chunk (current: 3)
  -max_recover_zeroes_gap <string> : 	The maximal length of zero bytes between non-zero bytes in a file that
  should be interpreted as downloaded during a recovery (current: 16)
  -file_completed_cmd <string> : 	A command that is called when a file is committed, does not work on MinGW.
  Arguments are (kept for compatibility):
    $1 - temp file name, without path
    $2 - file size
    $3 - filename of the committed file
  Also these environment variables can be used (preferred way):
    $TEMPNAME  - temp file name, including path
    $FILEID    - same as $1
    $FILESIZE  - same as $2
    $FILENAME  - same as $3
    $FILEHASH  - internal hash
    $DURATION  - download duration
    $INCOMING  - directory used for commit
    $NETWORK   - network used for downloading
    $ED2K_HASH - ed2k hash if MD4 is known
    $FILE_OWNER - user who started the download
    $FILE_GROUP - group the file belongs to
    $USER_MAIL - mail address of file_owner
   (current: )
  -file_started_cmd <string> : 	The command which is called when a download is started. Arguments
  are '-file <num>'
  Also these environment variables can be used (preferred way):
    $TEMPNAME  - temp file name, including path
    $FILEID    - same as $1
    $FILESIZE  - same as $2
    $FILENAME  - same as $3
    $FILEHASH  - internal hash
    $NETWORK   - network used for downloading
    $ED2K_HASH - ed2k hash if MD4 is known
    $FILE_OWNER - user who started the download
    $FILE_GROUP - group the file belongs to
    $USER_MAIL - mail address of file_owner
   (current: )
  -run_as_user <string> : 	The login of the user you want mldonkey to run as, after the ports
  have been bound (can be used not to run with root privileges when
  a port < 1024 is needed) (current: )
  -run_as_useruid <string> : 	The UID of the user (0=disabled) you want mldonkey to run as, after the ports
  have been bound (can be used not to run with root privileges when
  a port < 1024 is needed) (current: 0)
  -run_as_group <string> : 	The group of run_as_user user to be used (current: )
  -run_as_groupgid <string> : 	The group of run_as_user user to be used (current: 0)
  -ask_for_gui <string> : 	Ask for GUI start (current: false)
  -start_gui <string> : 	Automatically Start the GUI (current: false)
  -recover_temp_on_startup <string> : 	Should MLdonkey try to recover downloads of files in temp/ at startup (current: true)
  -config_files_security_space <string> : 	How many megabytes should MLdonkey keep for saving configuration files. (current: 10)
  -smtp_server <string> : 	The mail server you want to use (must be SMTP). Use hostname or IP address (current: 127.0.0.1)
  -smtp_port <string> : 	The port to use on the mail server (default 25) (current: 25)
  -mail <string> : 	Your e-mail if you want to receive mails when downloads are completed (current: )
  -add_mail_brackets <string> : 	Does your mail-server need <...> around addresses (current: false)
  -filename_in_subject <string> : 	Send filename in mail subject (current: true)
  -url_in_mail <string> : 	Put a prefix for the filename here which shows up in the notification mail (current: )
  -temp_directory <string> : 	The directory where temporary files should be put (current: temp)
  -share_scan_interval <string> : 	How often (in minutes) should MLDonkey scan all shared directories for new/removed files.
  Minimum 5, 0 to disable. Use command reshare to manually scan shares.
  When core starts, shared directories are scanned once, independent of this option. (current: 30)
  -create_file_mode <string> : 	New download files are created with these rights (in octal) (current: 664)
  -create_dir_mode <string> : 	New directories in incoming_directories are created with these rights (in octal) (current: 755)
  -create_file_sparse <string> : 	Create new files as sparse, only valid on MinGW for files on NTFS drives (current: true)
  -hdd_temp_minfree <string> : 	Mininum free space in MB on temp_directory, minimum 50 (current: 50)
  -hdd_temp_stop_core <string> : 	If true core shuts down when free space on temp dir is below hdd_temp_minfree,
  otherwise all downloads are paused and a warning email is sent. (current: false)
  -hdd_coredir_minfree <string> : 	Mininum free space in MB on core directory, minimum 20 (current: 50)
  -hdd_coredir_stop_core <string> : 	If true core shuts down when free space on core dir is below hdd_coredir_minfree,
  otherwise all downloads are paused and a warning email is sent. (current: true)
  -hdd_send_warning_interval <string> : 	Send a warning mail each <interval> hours for each directory, 0 to deactivate mail warnings. (current: 1)
  -previewer <string> : 	Name of program used for preview (first arg is local filename, second arg
  is name of file as searched on eDonkey (current: /usr/bin/see)
  -mldonkey_bin <string> : 	Directory where mldonkey binaries are installed (current: .)
  -mldonkey_gui <string> : 	Name of GUI to start (current: ./mlgui)
  -allow_any_command <string> : 	Allow you to use any command with ! in the interface instead of only the
  ones in allowed_commands (current: false)
  -allow_browse_share <string> : 	Allow others to browse our share list (0: none, 1: friends only, 2: everyone (current: 1)
  -messages_filter <string> : 	Regexp of messages to filter out, example: string1|string2|string3 (current: DI-Emule|ZamBoR|Ketamine|eMule FX|AUTOMATED MESSAGE|Hi Honey!|Do you live in my area|download HyperMule)
  -comments_filter <string> : 	Regexp of comments to filter out, example: string1|string2|string3 (current: http://|https://|www\.)
  -save_results <string> : 	(experimental) (current: 0)
  -buffer_writes <string> : 	Buffer writes and flush after buffer_writes_delay seconds (experimental) (current: false)
  -buffer_writes_delay <string> : 	Buffer writes and flush after buffer_writes_delay seconds (experimental) (current: 30.)
  -buffer_writes_threshold <string> : 	Flush buffers if buffers exceed buffer_writes_threshold kB (experimental) (current: 1024)
  -emule_mods_count <string> : 	build statistics about eMule mods (current: false)
  -emule_mods_showall <string> : 	show all eMule mods in statistics (current: false)
  -backup_options_delay <string> : 	How often (in hours) should a backup of the ini files be written into old_config.
  A value of zero means that a backup is written only when the core shuts down. (current: 0)
  -backup_options_generations <string> : 	Define the total number of options archives in old_config. (current: 10)
  -backup_options_format <string> : 	Define the format of the archive, zip or tar.gz are valid. (current: tar.gz)
  -shutdown_timeout <string> : 	The maximum time in seconds to wait for networks to cleanly shutdown. (current: 3)
  -interface_buffer <string> : 	The size of the buffer between the client and its GUI. Can be useful
  to increase when the connection between them has a small bandwith (current: 1000000)
  -max_name_len <string> : 	The size long names will be shortened to in the interface (current: 50)
  -max_filenames <string> : 	The maximum number of different filenames used by MLDonkey (current: 50)
  -max_client_name_len <string> : 	The size long client names will be shortened to in the interface (current: 25)
  -term_ansi <string> : 	Is the default terminal an ANSI terminal (escape sequences can be used) (current: true)
  -update_gui_delay <string> : 	Delay between updates to the GUI (current: 1.)
  -http_realm <string> : 	The realm shown when connecting with a WEB browser (current: MLdonkey)
  -html_frame_border <string> : 	This option controls whether the WEB interface should show frame borders or not (current: true)
  -commands_frame_height <string> : 	The height of the command frame in pixel (depends on your screen and browser sizes) (current: 46)
  -motd_html <string> : 	Message printed at startup additional to welcome text (current: )
  -compaction_delay <string> : 	Force compaction every <n> hours (in [1..24]) (current: 2)
  -vd_reload_delay <string> : 	The delay between reloads of the vd output in the WEB interface (current: 120)
  -client_bind_addr <string> : 	The IP address used to bind the p2p clients (current: 0.0.0.0)
  -create_mlsubmit <string> : 	Should the MLSUBMIT.REG file be created (current: true)
  -minor_heap_size <string> : 	Size of the minor heap in kB (current: 32)
  -relevant_queues <string> : 	The source queues to display in source lists (see 'sources' command) (current: 0 1 2 3 4 5 6 8 9 10 )
  -min_reask_delay <string> : 	The minimal delay between two connections to the same client (in seconds) (current: 600)
  -display_downloaded_results <string> : 	Whether to display results already downloaded (current: true)
  -filter_table_threshold <string> : 	Minimal number of results for filter form to appear (current: 50)
  -client_buffer_size <string> : 	Maximal size in byte of the buffers of a client, minimum 50.000 byte.
For high-volume links raise this value to 1.000.000 or higher. (current: 500000)
  -save_options_delay <string> : 	The delay between two saves of the 'downloads.ini' file (default is 15 minutes).
  Changes to this option require a core restart. (current: 900.)
  -server_connection_timeout <string> : 	timeout when connecting to a server (current: 30.)
  -download_sample_rate <string> : 	The delay between one glance at a file and another (current: 1.)
  -download_sample_size <string> : 	How many samples go into an estimate of transfer rates (current: 100)
  -compaction_overhead <string> : 	The percentage of free memory before a compaction is triggered (current: 25)
  -space_overhead <string> : 	The major GC speed is computed from this parameter. This is the memory
  that will be "wasted" because the GC does not immediatly collect 
  unreachable blocks. It is expressed as a percentage of the memory used
  for live data. The GC will work more (use more CPU time and collect 
  blocks more eagerly) if space_overhead is smaller. (current: 80)
  -max_displayed_results <string> : 	Maximal number of results displayed for a search (current: 1000)
  -options_version <string> : 	(internal option) (current: 21)
  -max_comments_per_file <string> : 	Maximum number of comments per file (current: 100)
  -max_comment_length <string> : 	Maximum length of file comments (current: 256)
  --ED2K-max_xs_packets <string> : 	Max number of UDP packets per round for eXtended Search (current: 30)
  --ED2K-port <string> : 	The port used for connection by other donkey clients. (current: 15579)
  --ED2K-check_client_connections_delay <string> : 	Delay used to request file sources (current: 180.)
  --ED2K-client_timeout <string> : 	Timeout on client connections when not queued (current: 40.)
  --ED2K-max_connected_servers <string> : 	The number of servers you want to stay connected to, maximum allowable = 3 (current: 3)
  --ED2K-reliable_sources <string> : 	Should mldonkey try to detect sources responsible for corruption and ban them, currently disabled (current: true)
  --ED2K-ban_identity_thieves <string> : 	Should mldonkey try to detect sources masquerading as others and ban them (current: true)
  --ED2K-server_black_list <string> : 	A list of server IP to remove from server list. Can contain single IPs, CIDR ranges, or begin-end ranges.
  Servers on this list can't be added, and will eventually be removed (current: )
  --ED2K-force_high_id <string> : 	immediately close connection to servers that don't grant a High ID (current: false)
  --ED2K-force_client_high_id <string> : 	send all clients your IP regardless of what ID was assigned by the server (current: false)
  --ED2K-update_server_list_server <string> : 	Set this option to false if you don't want to
  receive new servers from servers (current: false)
  --ED2K-update_server_list_server_met <string> : 	Set this option to false if you don't want to
  receive new servers from server.met (current: true)
  --ED2K-update_server_list_client <string> : 	Set this option to false if you don't want to
  receive new servers from clients (current: false)
  --ED2K-keep_best_server <string> : 	Set this option to false if you don't want mldonkey
  to change the master servers it is connected to (current: true)
  --ED2K-connect_only_preferred_server <string> : 	only servers which are set to 'preferred' will be connected, 
  please note that any server must be set to preferred to use this feature (current: false)
  --ED2K-max_walker_servers <string> : 	Number of servers that can be used to walk
  between servers (current: 1)
  --ED2K-walker_server_lifetime <string> : 	The maximal delay a connection with a server should last when walking through the list (current: 300)
  --ED2K-log_clients_on_console <string> : 	 (current: false)
  --ED2K-propagate_sources <string> : 	Allow mldonkey to propagate your sources to other mldonkey clients.
   This function is superseded by eMule-style source exchange,
   this option is outdated (current: false)
  --ED2K-max_sources_per_file <string> : 	Maximal number of sources for each file (current: 5000)
  --ED2K-keep_sources <string> : 	Save sources to file_sources.ini and reload them on core start. (current: true)
  --ED2K-client_md4 <string> : 	The MD4 of this client (current: E7582BD7590EB7A80741FDAD53B36F90)
  --ED2K-client_private_key <string> : 	The RSA private key of this client (current: MIIBCAIBADANBgkqhkiG9w0BAQEFAASB8zCB8AIBAAIxAKSj6JwZBQ+VUDGMca2mM+xek1IsSNflKesQ2zG3GsECgPEcVxQiMwucuDLbpaWFtQIBEQIwBAkJd0bjQuRJfrRiKKX0uT6Npmf8xAlhnZGS0beJg2GB0Eb+uIW0BDHuzBSSg0PZAhkA11DcRBE2i5EiyYXYwag8x8tf976KfRxJAhkAw7/L55jGeE16j3kY6fer/w9JTjwn1mYNAhg/VASMfYiDZuwdJ13emuSzO9Dug1Xoj9kCGFCaRObkjfVNFFkx3RULoSzKDyA24zoqBQIZALRNXQVqp5h5z67LYRKZjoVx0G/iYmZDvA==)
  --ED2K-enable_sui <string> : 	Enable secure user identification support (current: true)
  --ED2K-black_list <string> : 	 (current: true)
  --ED2K-queued_timeout <string> : 	How long should we wait in the queue of another client (current: 1800.)
  --ED2K-upload_timeout <string> : 	How long can a silent client stay in the upload queue (current: 600.)
  --ED2K-upload_lifetime <string> : 	How long a downloading client can stay in my upload queue (in minutes >5) (current: 90)
  --ED2K-upload_full_chunks <string> : 	If true, each client is allowed to receive one chunk, this setting overrides upload_lifetime (current: true)
  --ED2K-upload_complete_chunks <string> : 	If true, each client is allowed to complete only one chunk, independent, if it is empty or
   partial. this setting overrides upload_full_chunks and dynamic_upload_lifetime,
   but is, as a failsafe, limited by upload_lifetime (should be set reasonable high) (current: false)
  --ED2K-dynamic_upload_lifetime <string> : 	Each client upload lifetime depends on download-upload ratio (current: false)
  --ED2K-dynamic_upload_threshold <string> : 	Uploaded zones (1 zone = 180 kBytes) needed to enable the dynamic upload lifetime (current: 10)
  --ED2K-connected_server_timeout <string> : 	How long can a silent server stay connected (current: 1800.)
  --ED2K-upload_power <string> : 	The weight of upload on a donkey connection compared to upload on other
  peer-to-peer networks. Setting it to 5 for example means that a donkey
  connection will be allowed to send 5 times more information per second than
  an Open Napster connection. This is done to favorise donkey connections
  over other networks, where upload is less efficient, without preventing
  upload from these networks. (current: 5)
  --ED2K-max_server_age <string> : 	max number of days after which an unconnected server is removed (current: 2)
  --ED2K-remove_old_servers_delay <string> : 	How often should remove old donkey servers (see max_server_age) be called
  (in seconds, 0 to disable) (current: 900.)
  --ED2K-min_left_servers <string> : 	Minimal number of servers remaining after remove_old_servers (current: 20)
  --ED2K-servers_walking_period <string> : 	How often should we check all servers (minimum 4 hours, 0 to disable) (current: 6)
  --ED2K-keep_cancelled_in_old_files <string> : 	Are the cancelled files added to the old files list to prevent re-download ? (current: false)
  --ED2K-keep_downloaded_in_old_files <string> : 	Are the downloaded files added to the old files list to prevent re-download ? (current: false)
  --ED2K-send_warning_messages <string> : 	true if you want your mldonkey to lose some
  upload bandwidth sending messages to clients which are banned :) (current: false)
  --ED2K-ban_queue_jumpers <string> : 	true if you want your client to ban
  clients that try queue jumping (3 reconnections faster than 9 minutes) (current: true)
  --ED2K-use_server_ip <string> : 	true if you want your client IP to be set from servers ID (current: true)
  --ED2K-ban_period <string> : 	Set the number of hours you want client to remain banned (current: 1)
  --ED2K-good_client_rank <string> : 	Set the maximal rank of a client to be kept as a client (current: 500)
  --ED2K-min_users_on_server <string> : 	disconnect if server users is smaller (current: 0)
  --ED2K-max_published_files <string> : 	maximum number of files published to servers per minute, eMule default 200 (current: 200)
  --ED2K-login <string> : 	login of client on eDonkey network (nothing default to global one) (current: )
  --ED2K-options_version <string> : 	(internal option) (current: 4)
  --BT-client_port <string> : 	The port to bind the client to (current: 6882)
  --BT-client_uid <string> : 	The UID of this client (current: FVGUYMZOGAXDALII7OQLQ7KVS3VMCVKU)
  --BT-ask_tracker_threshold <string> : 	Ask the tracker for new sources only if you have fewer than that number of sources (current: 20)
  --BT-max_tracker_redirect <string> : 	Maximum number of HTTP redirects before reaching the tracker - maximum 10, 0 to disable (current: 1)
  --BT-send_key <string> : 	Send client key to trackers (current: true)
  --BT-max_uploaders_per_torrent <string> : 	Maximum number of uploaders for one torrent, can not be higher than max_bt_uploaders (current: 3)
  --BT-max_bt_uploaders <string> : 	Maximum number of uploaders for bittorrent, can not be higher than max_upload_slots (current: 3)
  --BT-numwant <string> : 	Number of peers to request from tracker (Negative # = let tracker decide) (current: -1)
  --BT-import_new_torrents_interval <string> : 	Interval in seconds 'torrents/incoming' is scanned for new torrent files to be downloaded, 0 to deactivate (current: 60.)
  --BT-tracker_retries <string> : 	Number of retries before a tracker is disabled, use 0 to not disable trackers (current: 10)
  --BT-min_tracker_reask_interval <string> : 	Minimum time in seconds to wait between asking the tracker for sources (current: 300)
  --BT-client_timeout <string> : 	Timeout on client connections (current: 90.)
  --BT-user_agent <string> : 	User agent string (default = "default") (current: default)
  --BT-options_version <string> : 	(internal option) (current: 0)
  --BT-tracker_port <string> : 	The port to bind the tracker to (current: 6881)
  --BT-max_tracked_files <string> : 	The maximal number of tracked files (to prevend saturation attack) (current: 100)
  --BT-max_tracker_reply <string> : 	The maximal number of peers returned by the tracker (current: 20)
  --BT-tracker_force_local_torrents <string> : 	The tracker will check the torrent file is available if an announce request is received (current: true)
  --BT-tracker_use_key <string> : 	The tracker will check the client key to update ip if changed (current: true)
  --BT-default_tracker <string> : 	Let you define a default tracker for creating torrents (leave empty for mlnet tracker) (current: )
  --BT-default_comment <string> : 	Let you define a default comment for creating torrents (current: )
  --FTP-get_range <string> : 	The command to call to get a range (current: get_range)
  --FTP-range_arg <string> : 	The argument to !!get_range to get a range (current: range)
  --FTP-options_version <string> : 	(internal option) (current: 0)
  --FTP-chunk_size <string> : 	Chunk size (in bytes) (0 = No chunks) (current: 0)
  --DC-client_port <string> : 	The port to bind the client to (current: 4444)
  --DC-dc_open_slots <string> : 	How many slots are open to other clients (current: 2)
  --DC-login <string> : 	Your login on DC (no spaces !!!) (current: )
  --DC-servers_list_url <string> : 	The URL from which the first server list is downloaded (current: http://dchublist.com/hublist.config.bz2)
  --DC-search_timeout <string> : 	The time a search is active (current: 60)
  --DC-file_search_time <string> : 	Minimum time between automated file tth searches in minutes (current: 60)
  --DC-client_timeout <string> : 	In initialization, the time in seconds we wait connection responses from client (current: 90)
  --DC-client_read_timeout <string> : 	The maximum time in seconds we wait data from client in downloading (current: 60)
  --DC-client_write_timeout <string> : 	The maximum time in seconds we wait to continue pushing data to client in uploading (current: 60)
  --DC-wait_for_next_upload <string> : 	How many seconds we wait a client to continue slot before closing and freeing the slot (current: 30)
  --DC-firewalled <string> : 	Is this client firewalled (use passive searches) (current: false)
  --DC-autosearch_by_tth <string> : 	Automatically find alternative sources, if file is unavailable. Also add sources
     from searches automatically (current: true)
  --DC-max_sources_file <string> : 	Maximal number of sources to single file (current: 5)
  --DC-client_speed <string> : 	The line speed sent in the MyINFO message
     eg. Modem, Cable, DSL, Satellite, LAN(T1), LAN(T3) (current: DSL)
  --DC-options_version <string> : 	(internal option) (current: 0)
  -dump  <filename> : dump file
  -known  <filename> : print a known.met file
  -part  <filename> : print a .part.met file
  -server  <filename> : print a server.met file
  -pref  <filename> : print a server.met file
  -peers  <filename> : print a contact.dat file
  -help : Display this list of options
  --help : Display this list of options
