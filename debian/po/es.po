# mldonkey po-debconf translation to Spanish
# Copyright (C) 2007, 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the mldonkey package.
#
# Changes:
#   - Initial translation
#       Manuel Porras Peralta <venturi.debian@gmail.com>, 2007
#
#   - Updates
#       Francisco Javier Cuadrado <fcocuadrado@gmail.com>, 2009
#
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
#       info -n '(gettext)PO Files'
#       info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
#   - El proyecto de traducción de Debian al español
#     http://www.debian.org/intl/spanish/
#     especialmente las notas y normas de traducción en
#     http://www.debian.org/intl/spanish/notas
#
#   - La guía de traducción de po's de debconf:
#     /usr/share/doc/po-debconf/README-trans
#     o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
msgid ""
msgstr ""
"Project-Id-Version: mldonkey 3.0.0-2\n"
"Report-Msgid-Bugs-To: mldonkey@packages.debian.org\n"
"POT-Creation-Date: 2011-05-07 15:41+0200\n"
"PO-Revision-Date: 2009-04-07 09:37+0100\n"
"Last-Translator: Francisco Javier Cuadrado <fcocuadrado@gmail.com>\n"
"Language-Team: Debian l10n Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../mldonkey-server.templates:1001
msgid "Launch MLDonkey at startup?"
msgstr "¿Desea ejecutar MLDonkey al inicio del sistema?"

#. Type: boolean
#. Description
#: ../mldonkey-server.templates:1001
msgid ""
"If enabled, each time your machine starts, the MLDonkey server will be "
"started."
msgstr ""
"Si está activado, cada vez que su máquina arranque, se iniciará el servidor "
"MLDonkey."

#. Type: boolean
#. Description
#: ../mldonkey-server.templates:1001
msgid ""
"Otherwise, you will need to launch MLDonkey yourself each time you want to "
"use it."
msgstr ""
"De otro modo, tendrá que iniciar MLDonkey usted mismo cada vez que desee "
"utilizarlo."

#~ msgid "Bug #200500"
#~ msgstr "Error 200500"

#~ msgid ""
#~ "Previous versions of mldonkey-server suffer from a serious DFSG policy "
#~ "violation."
#~ msgstr ""
#~ "Las versiones anteriores de mldonkey-server violan gravemente la política "
#~ "de las DFSG."

#~ msgid ""
#~ "The plugin for the fasttrack protocol (e.g. used by kazaa) of mldonkey-"
#~ "server was made with illegal coding practice. This version fixes the "
#~ "problem by removing this plugin from the MLDonkey package. Any fasttrack "
#~ "sources will be filtered out of your files.ini."
#~ msgstr ""
#~ "El complemento para el protocolo «fasttrack» (por ejemplo: kazaa lo "
#~ "utiliza) de mldonkey-server se hizo con prácticas de código ilegales. "
#~ "Esta versión arregla el problema eliminando este complemento del paquete "
#~ "MLDonkey. Cualquier fuente de «fasttrack» se eliminará del archivo «files."
#~ "ini»."

#~ msgid ""
#~ "Your entire fasttrack upload will disappear with the next restart of the "
#~ "mldonkey server."
#~ msgstr ""
#~ "Todos sus subidas mediante «fasttrack» desaparecerán la próxima vez que "
#~ "inicie el servidor mldonkey."

#~ msgid ""
#~ "See /usr/share/doc/mldonkey-server/README.Debian for more information."
#~ msgstr ""
#~ "Para más información, lea «/usr/share/doc/mldonkey-server/README.Debian»."

#~ msgid "MLDonkey user:"
#~ msgstr "Usuario de MLDonkey:"

#~ msgid "Define the user who will run the MLDonkey server process."
#~ msgstr "Defina el usuario que ejecutará el servidor MLDonkey."

#~ msgid ""
#~ "Please do not choose a real user. For security reasons it is better if "
#~ "this user does not own any other data than the MLDonkey share."
#~ msgstr ""
#~ "Por favor, no elija un usuario real. Por razones de seguridad es mejor si "
#~ "este usuario no tiene ningún dato más que los compartidos por MLDonkey."

#~ msgid ""
#~ "You will use this user account to share and get data from the peer-to-"
#~ "peer networks."
#~ msgstr ""
#~ "Usará esta cuenta de usuario para compartir y obtener datos de redes p2p."

#~ msgid ""
#~ "This user will be a system user (if created). You won't be able to login "
#~ "into your system with this user name."
#~ msgstr ""
#~ "Este usuario será un usuario del sistema (si se crea). No podrá iniciar "
#~ "sesión en su sistema con este nombre de usuario."

#~ msgid "MLDonkey group:"
#~ msgstr "Grupo de MLDonkey:"

#~ msgid "Define the group which will run the MLDonkey server process."
#~ msgstr "Defina el grupo que ejecutará el servidor MLDonkey."

#~ msgid ""
#~ "Please do not choose a real group. For security reasons it is better if "
#~ "this group does not own any other data than the MLDonkey share."
#~ msgstr ""
#~ "Por favor, no elija un grupo real. Por razones de seguridad es mejor si "
#~ "este grupo no tiene ningún dato más que los compartidos por MLDonkey."

#~ msgid ""
#~ "Users of this group can start and stop the MLDonkey server and can also "
#~ "access the files fetched from the peer-to-peer networks."
#~ msgstr ""
#~ "Los usuarios de este grupo pueden iniciar y detener el servidor MLDonkey "
#~ "y también pueden acceder a los archivos descargados de redes p2p."

#~ msgid "Change the owner of old files?"
#~ msgstr "¿Cambiar el propietario de los archivos antiguos?"

#~ msgid ""
#~ "You have changed the MLDonkey user. You can change the ownership of your "
#~ "files to the new user."
#~ msgstr ""
#~ "Ha cambiado el usuario de MLDonkey. Puede cambiar la propiedad de sus "
#~ "archivos al nuevo usuario."

#~ msgid ""
#~ "PS: the former user won't be deleted from /etc/passwd, you will have to "
#~ "do it yourself later (e.g. with deluser(8)), or you keep it along with "
#~ "the old configuration."
#~ msgstr ""
#~ "NOTA: el usuario en cuestión no se borrará de «/etc/passwd», tendrá que "
#~ "hacerlo usted mismo más tarde (por ej. con deluser(8)), o mantenerlo con "
#~ "la antigua configuración."

#~ msgid "MLDonkey directory:"
#~ msgstr "Directorio de MLDonkey:"

#~ msgid ""
#~ "Define the directory to which the MLDonkey server will be chdired and "
#~ "chrooted."
#~ msgstr ""
#~ "Defina el directorio del servidor MLDonkey al que se le aplicará chdir y "
#~ "chroot."

#~ msgid ""
#~ "The .ini configuration files, incoming and shared directories will be in "
#~ "this directory."
#~ msgstr ""
#~ "Los archivos .ini de configuración y las carpetas «incoming» y «shared», "
#~ "estarán en dicho directorio."

#~ msgid ""
#~ "Chroot support is not complete. For now, chroot is not possible, but it "
#~ "may be enabled in the near future."
#~ msgstr ""
#~ "El soporte para chroot no está terminado. Por ahora, chroot no es "
#~ "posible, pero podrá activarse en el futuro."

#~ msgid "Move the old configuration?"
#~ msgstr "¿Mover la configuración antigua?"

#~ msgid ""
#~ "You have changed the mldonkey directory. You can move the old files to "
#~ "this new directory."
#~ msgstr ""
#~ "Ha cambiado el directorio mldonkey. Puede mover los archivos antiguos a "
#~ "este nuevo directorio."

#~ msgid ""
#~ "If you choose no, the old directory won't be deleted. You will have to do "
#~ "it yourself."
#~ msgstr ""
#~ "Si elige no, no se borrará el directorio antiguo. Tendrá que hacerlo "
#~ "usted mismo."

#~ msgid "Niceness of MLDonkey:"
#~ msgstr "Eficiencia de MLDonkey:"

#~ msgid ""
#~ "MLDonkey uses heavy calculation from time to time (like hashing very big "
#~ "files). It should be a good idea to set a very kind level of niceness, "
#~ "depending on what ressources you want to give to MLDonkey."
#~ msgstr ""
#~ "MLDonkey usa cálculos exhaustivos cada cierto tiempo (como ordenar por "
#~ "«hash» archivos muy grandes). Sería una buena idea establecer un nivel "
#~ "adecuado de eficiencia, dependiendo de los recursos que desee dar a "
#~ "MLDonkey."

#~ msgid ""
#~ "You can set values from -20 to 20. The bigger the niceness, the lower the "
#~ "priority of MLDonkey processes."
#~ msgstr ""
#~ "Puede establecer los valores de -20 a 20. Cuanto mayor es la eficiencia, "
#~ "menor es la prioridad de los procesos de MLDonkey."

#~ msgid "Maximal download speed (kB/s):"
#~ msgstr "Velocidad máxima de descarga (KB/s):"

#~ msgid ""
#~ "Set the maximal download rate. It can be useful to limit this rate, in "
#~ "order to always have a minimal bandwidth for other internet applications."
#~ msgstr ""
#~ "Establece el valor de la velocidad de descarga máxima. Puede ser útil "
#~ "limitar este valor, para que siempre disponga de un ancho de banda mínimo "
#~ "para otras aplicaciones de internet."

#~ msgid ""
#~ "It has also been noticed that a full use of the bandwidth could cause "
#~ "problems with DSL connection handling. This is not a rule, it is just "
#~ "based on a few experiments."
#~ msgstr ""
#~ "También se ha observado que un uso completo del ancho de banda puede "
#~ "causar problemas con conexiones de ADSL. Esto no es oficial, es sólo "
#~ "basado en algunos experimentos."

#~ msgid "0 means no limit."
#~ msgstr "0 indica ilimitado."

#~ msgid "Maximal upload speed (kB/s):"
#~ msgstr "Velocidad máxima de envío (KB/s):"

#~ msgid ""
#~ "Set the maximal upload rate. You must keep in mind that a peer-to-peer "
#~ "network is based on sharing. Do not use a very low rate."
#~ msgstr ""
#~ "Establece el valor de la velocidad máxima de envío. Tenga en cuenta que "
#~ "una red p2p se basa en compartir. No use un valor demasiado bajo."

#~ msgid ""
#~ "Some networks calculate the download credit by the upload rate. More "
#~ "upload speed means more download speed."
#~ msgstr ""
#~ "Algunas redes calculan el crédito de descarga por el valor del envío. Más "
#~ "velocidad de envío se traduce en una mayor velocidad de descarga."

#~ msgid ""
#~ "As for the download speed, you should limit this rate so that you can "
#~ "still use the internet even when MLDonkey is running."
#~ msgstr ""
#~ "Como con la velocidad de descarga, puede limitar este valor por si usa "
#~ "internet aún con MLDonkey ejecutándose."

#~ msgid "Password of admin user:"
#~ msgstr "Contraseña del administrador:"

#~ msgid ""
#~ "As of version 2.04rc1, a new user management appears. The password is "
#~ "encrypted and stored in downloads.ini."
#~ msgstr ""
#~ "Aparece un nuevo gestor de usuarios, como en la versión 2.04rc1. La "
#~ "contraseña se codifica y se guarda en «downloads.ini»"

#~ msgid ""
#~ "If you want to add a new user for MLDonkeys user management or want to "
#~ "change the password, refer to /usr/share/doc/mldonkey-server/README."
#~ "Debian."
#~ msgstr ""
#~ "Si desea añadir un nuevo usuario al gestor de usuarios de MLDonkey o "
#~ "desea cambiar la contraseña, lea «/usr/share/doc/mldonkey-server/README."
#~ "Debian» atentamente."

#~ msgid "Retype password of the admin user:"
#~ msgstr "Vuelva a escribir la contraseña del administrador:"

#~ msgid "Please confirm your admin's password."
#~ msgstr "Por favor, confirme su contraseña de administrador."

#~ msgid "Passwords do not match"
#~ msgstr "Las contraseñas no coinciden"

#~ msgid "The two password you enter must be the same."
#~ msgstr "Las dos contraseñas que introduzca deben ser la misma."

#~ msgid "You will be asked until you can provide the same password twice."
#~ msgstr ""
#~ "Se le preguntará hasta que introduzca la misma contraseña dos veces."

#~ msgid "Maximal time to live for the server:"
#~ msgstr "Máximo tiempo de conexión para el servidor:"

#~ msgid ""
#~ "MLDonkey needs to be restarted from time to time. This is safer for "
#~ "memory consumption and all. You need to set the time between automatic "
#~ "restarts (in hours)."
#~ msgstr ""
#~ "MLDonkey necesita reiniciarse cada cierto tiempo. Es más seguro para el "
#~ "consumo de memoria y para todo. Tiene que establecer el tiempo entre los "
#~ "reinicios automáticos (en horas)."

#~ msgid ""
#~ "The only problem with this is that you will loose some upload credits. "
#~ "You can set this variable to a very high value in order to be sure to "
#~ "have enough time to gather a lot of credits."
#~ msgstr ""
#~ "El único problema con esto es que perderá algunos créditos de envío. "
#~ "Puede establecer esta variable al valor más alto para estar seguro de "
#~ "tener tiempo suficiente para reunir muchos créditos."

#~ msgid "For example: 24 for one day, 168 for one week."
#~ msgstr "Por ejemplo: 24 horas para un día, 168 para una semana."
